package sbu.cs.excercises;
import sbu.cs.IllegalValueException;
import org.apache.commons.lang3.RandomStringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ExerciseLecture5 {

    /**
     *   implement a function to create a random password with
     *   given length using lower case letters
     *   lecture 5 page 14
     * @param length length of the wanted password
     */
    public String weakPassword(int length)
    {
        String ans = "";

        int min = 97;  // letter 'a'

        int max = 122;  // letter 'z'

        Random random = new Random();

        for (int i = 0 ; i < length ; i++)
        {
            int randomNum = (int) ((Math.random() * (max - min)) + min);

            ans += (char) randomNum;
        }


        return ans;
    }

    /**
     *   implement a function to create a random password with
     *   given length and at least 1 digit and 1 special character
     *   lecture 5 page 14
     * @param length length of the wanted password
     */
    public String strongPassword(int length) throws IllegalValueException
    {
        if (length > 2)
        {
            StringBuilder ans = new StringBuilder(RandomStringUtils.randomAlphabetic(length)) ; // creating an alphaNumeric string without special characters
            String specialChars = "!@#$%^&*()_+-=`~";
            String digits = "0123456789";
            Random random = new Random();
            int numOfSpecialChars = (int) ((Math.random() * (length - 3))); // randomly deciding the num of specials
            int numOfDigits = (int) ((Math.random() * (length - 3)) ); // randomly deciding num of digits
            ans.setCharAt(0,specialChars.charAt((int) (Math.random() * (13)))); // making sure a special exists
            ans.setCharAt(1,digits.charAt((int) (Math.random() * 10))); // making sure a digit exists
            for (int i = 2 ; i < numOfSpecialChars ; i++) // setting special chars
            {
                int temp = (int) ((Math.random() * (13)));
                ans.setCharAt(i,specialChars.charAt(temp));
            }
            for (int i = numOfSpecialChars + 2  ; i < numOfSpecialChars + numOfDigits ; i++) // setting digits
            {
                int temp = (int) ((Math.random() * (9)));
                ans.setCharAt(i,digits.charAt(temp));
            }
            String finalAns = ans.toString();
            finalAns = finalAns.toLowerCase();
            return finalAns;
        }
        else
        {
            throw new IllegalValueException();
        }

    }

    /**
     *   implement a function that checks if an integer is a fibobin number
     *   integer n is fibobin if there exist an i where:
     *       n = fib(i) + bin(fib(i))
     *   where fib(i) is the ith fibonacci number and bin(i) is the number
     *   of ones in binary format
     *   lecture 5 page 17
     * @param n the number n
     */
    public boolean isFiboBin(int n)
    {
        boolean ans = false;

        long fib1 = 1;
        long fib2 = 1;

        for (int i = 0 ; i < n - 1  ; i++)
        {
            long temp = fib1;

            fib1 = fib2;

            fib2 += temp;
        }

        // now fib1 or fib2 is the nth fib

        long nthFib = 0;

        if (n % 2 == 0)
        {
            nthFib = fib1;
        }
        else
        {
            nthFib = fib2;
        }

        // taking fib1 to binary

        String binFibAsString = Integer.toBinaryString((int)fib1);

        int numberOfOnes = 0;

        for (int i = 0 ; i < binFibAsString.length() ; i++) // counting the number of ones
        {
            if (binFibAsString.charAt(i) == '1')
            {
                numberOfOnes++;
            }
        }

        if (n == numberOfOnes + nthFib)
        {
            ans = true;
        }

        return ans;
    }
}
