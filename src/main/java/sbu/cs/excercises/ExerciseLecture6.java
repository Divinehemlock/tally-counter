package sbu.cs.excercises;



import java.util.ArrayList;
import java.util.List;

public class ExerciseLecture6 {

    /**
     *   implement a function that takes an array of int and return sum of
     *   elements at even positions
     *   lecture 6 page  16
     * @param array an integer array
     */
    public long calculateEvenSum(int[] array)
    {
        long ans = 0;

        for (int i = 0 ; i < array.length ; i += 2)
        {
            ans += array[i];
        }

        return ans;
    }

    /**
     *   implement a function that takes an array of int and return that
     *   array in reverse order
     *   lecture 6 page 16
     * @param array an integer array
     */
    public int[] reverseArray(int[] array)
    {
        int[] ans = new int[array.length];

        for (int i = array.length - 1 ; i >= 0 ; i--)
        {
            ans[array.length - i - 1 ] = array[i];
        }

        return ans;
    }

    /**
     *   implement a function that calculate product of two 2-dim matrices
     *   lecture 6 page 21
     * @param matrix1 the first matrix
     * @param matrix2 the second matrix
     */
    public double[][] matrixProduct(double[][] matrix1, double[][] matrix2) throws RuntimeException
    {
        int mat1Rows = matrix1.length;
        int mat1Cols = matrix1[0].length;
        int mat2Rows = matrix2.length;
        int mat2Cols = matrix2[0].length;
        if (mat1Cols == mat2Rows)
        {
            double[][] ans = new double[mat1Rows][mat2Cols];
            for(int i = 0; i < mat1Rows; i++)
            {
                for(int j = 0; j < mat2Cols; j++)
                {
                    ans[i][j]=0;
                    for(int k = 0; k < mat1Cols; k++)
                    {
                        ans[i][j] += matrix1[i][k] * matrix2[k][j];
                    }
                }
            }
            return ans;
        }
        else
        {
            throw new RuntimeException();
        }

    }

    /**
     *   implement a function that return array list of array list of string
     *   from a 2-dim string array
     *   lecture 6 page 30
     * @param names a two-dimensional array of strings
     */
    public List<List<String>> arrayToList(String[][] names)
    {
        int length = names.length;
        List<List<String>> listOfLists = new ArrayList<List<String>>(); // making the list of lists
        for (int i = 0 ; i < length ; i++)
        {
            List<String> tempList = new ArrayList<>();
            for (int j = 0 ; j < names[i].length ; j++)// copying string elements into a list
            {
                tempList.add(names[i][j]);
            }
            listOfLists.add((tempList)); // adding the list to the listOfLists
        }
        return listOfLists;
    }

    /**
     *   implement a function that return a list of prime factor of integer n
     *   in ascending order
     *   lecture 6 page 30
     * @param n the number n
     */
    public List<Integer> primeFactors(int n)
    {
        List<Integer> ans = new ArrayList<Integer>();
        int factor = 2;
        int step = 1;
        while (n > 1)
        {
            if (n % factor == 0)
            {
                ans.add(factor);
                n /= factor;
                while (n % factor == 0)
                {
                    n /= factor;
                }
            }
            factor += step;
            step = 2;
        }
        return ans;
    }

    /**
     *   implement a function that return a list of words in a given string.
     *   consider that words are separated by spaces, commas,questions marks....
     *   lecture 6 page 30
     */
    public List<String> extractWord(String line)
    {
        List<String> ans = new ArrayList<>();
        String[] broken = line.split(" "); // breaking the string
        for (String s : broken)
        {
            StringBuilder temp = new StringBuilder(s);
            for (int i = 0 ; i < s.length() ; i++) // removing non-alphabetical chars except for underscore and dash
            {
                if((temp.charAt(i) < 97 || temp.charAt(i) > 122) && temp.charAt(i) != 45 && temp.charAt(i) != 95 )
                {
                    if (temp.charAt(i) < 65 || temp.charAt(i) > 90)
                    {
                        temp.setCharAt(i,' ');
                    }

                }
            }
            s = temp.toString();
            s = s.replaceAll("\\s","");
            ans.add(s);
        }
        
        return ans;
    }
}
