package sbu.cs.excercises;

import sbu.cs.IllegalValueException;

import java.util.Locale;

public class ExerciseLecture4 {

    /**
     *   implement a function that returns factorial of given n
     *   lecture 4 page 15
     * @exception IllegalValueException when the factorial of n doesn't exist
     * @param n an integer which the function should return its factorial, namely
     *          n!
     */
    public long factorial(int n) throws IllegalValueException {

        if (n >= 0)
        {
            int ans = 1;

            for (int i = n ; i >= 1 ; i--)
            {
                ans *= i;
            }

            return ans;
        }
        else
        {
            throw new IllegalValueException();
        }

    }

    /**
     *   implement a function that return nth number of fibonacci series
     *   the series -> 1, 1, 2, 3, 5, 8, ...
     *   lecture 4 page 19
     * @param n an integer that indicated the nth number in fibonacci series
     */
    public long fibonacci(int n)
    {
        long fib1 = 1;
        long fib2 = 1;

        for (int i = 0 ; i < n - 1 ; i++)
        {
            long temp = fib1;

            fib1 = fib2;

            fib2 += temp;
        }

        return fib1;

    }

    /**
     *   implement a function that return reverse of a given word
     *   lecture 4 page 19
     * @param word the word as a string
     */
    public String reverse(String word)
    {
        char[] ansAsChar = new char[word.length()];

        int index = 0; // used to keep track of ansAsChar indices

        for (int i = word.length() - 1 ; i >= 0 ; i--)
        {
            ansAsChar[index] = word.charAt(i);

            index++;
        }

        return String.valueOf(ansAsChar);
    }

    /**
     *   implement a function that returns true if the given line is
     *   palindrome and false if it is not palindrome.
     *   palindrome is like 'wow', 'never odd or even', 'Wow'
     *   lecture 4 page 19
     *
     * @param line the line of text as a string
     */
    public boolean isPalindrome(String line)
    {
        boolean ans = true;

        line = line.toLowerCase(); // ignoring lower or upper case differences

        line = line.replaceAll("\\s", ""); // removing white spaces

        for (int i = 0 ; i < line.length() / 2 ; i++)
        {
            if (line.charAt(i) != line.charAt(line.length() - 1 - i))
            {
                ans = false;

                break;
            }
        }

        return ans;
    }

    /**
     *   implement a function which computes the dot plot of 2 given
     *   string. dot plot of hello and hello is:
     *       h e l l o
     *   h   *
     *   e     *
     *   l       * *
     *   l       * *
     *   o           *
     *   lecture 4 page 26
     * @param string1 the first string
     * @param string2 the second string
     */
    public char[][] dotPlot(String string1, String string2) {

        char[][] ans = new char[string1.length()][string2.length()];

        for (int i = 0 ; i < string1.length() ; i++)
        {
            for (int j = 0 ; j < string2.length() ; j++)
            {
                if (string1.charAt(i) == string2.charAt(j))
                {
                    ans[i][j] = '*';
                }
                else
                {
                    ans[i][j] = ' ';
                }
            }
        }
        return ans;
    }
}
