package sbu.cs.excercises;

import sbu.cs.IllegalValueException;
import sbu.cs.TallyCounterInterface;

/**
 * Implement the following functions and add fields
 * to the class below so that it resembles the functionality
 * of a real tally counter.
 * Consider a tally counter which can count up to 9999.
 */
public class TallyCounter implements TallyCounterInterface {

    private int value = 0;

    /**
     * Increments the counter of tally counter by one
     */
    @Override
    public void count()
    {
        int currentValue = getValue();

        if (currentValue <= 9998 && currentValue >= 0)
        {
            this.value++;
        }

    }

    /**
     * @return the current value of counter
     */
    @Override
    public int getValue()
    {
        return this.value;
    }

    /**
     * Sets a new value for counter
     * @param newCounterValue the new value
     * @throws IllegalValueException whenever the input value is not valid.
     * think of the scenarios in which the value is unacceptable
     */
    @Override
    public void setValue(int newCounterValue) throws IllegalValueException
    {
        if (newCounterValue <= 9999 && newCounterValue >= 0)
        {
            this.value = newCounterValue;
        }
        else
        {
            throw new IllegalValueException();
        }

    }

    /**
     * resets the counter
     */
    @Override
    public void reset()
    {
        this.value = 0;
    }
}
